/*
create = function(this, surfaces) {
	ds_list_add(surfaces, 
		surface_create(room_width, room_height),
	);
	this.isRedraw = true;
}


step = function(this, surface, index) {
	if(!this.isRedraw) {
		draw_clear_alpha(c_black, 0);
		draw_surface(application_surface, camera_get_view_x(view_camera[0]), camera_get_view_y(view_camera[0]));
	}
}

draw = function(this, surface, index) {
	if (!this.isRedraw) {
		gpu_set_blendmode_ext(bm_one, bm_zero);
		for (var i = 0; i < room_width; i += 2)
			draw_surface_part(surface, i * 2, 0, 2, room_height, i * 2, random_range(-5, 5));
			
		gpu_set_blendmode(bm_normal);
		this.isRedraw = true;
	}
	else {
		this.isRedraw = false;
	}
}

/*
step = function(this, surface, index) {
	draw_clear_alpha(c_black, 0);
	draw_surface(application_surface, camera_get_view_x(view_camera[0]), camera_get_view_y(view_camera[0]));
}

draw = function(this, surface, index) {
	return;
}

drawGUI = function(this, surface, index) {
	gpu_set_blendmode_ext(bm_one, bm_zero);
	var dx = camera_get_view_x(view_camera[0]);
	var dy = camera_get_view_y(view_camera[0]);
	//for (var i = dx; i < dx + camera_get_view_width(view_camera[0]); i += 2)
	//	draw_surface_part(surface, i * 2, 0, 2, dy, i * 2, random_range(-5, 5));
	for (var i = 0; i < room_width; i += 2)
		draw_surface_part(surface, i * 2, 0, 2, room_height, i * 2, random_range(-5, 5));
			
	gpu_set_blendmode(bm_normal);
	draw_set_font(fHannotate24);
	draw_text(32, 64, [dx, camera_get_view_width(view_camera[0])]);
}
*/
//*/

create = function(this, surfaces) {
	ds_list_add(surfaces, 
		surface_create(room_width, room_height),
		surface_create(room_width, room_height),
		surface_create(room_width, room_height),
	);
	this.isRedraw = true;
	this.samePlace = false;
	this.scale = 2;
	this.radius = 50;
}

step = function(this, surface, index) {
	if (index == 1) {
		if (!this.isRedraw) {
			draw_clear_alpha(c_black, 0);
			draw_surface(application_surface, camera_get_view_x(view_camera[0]), camera_get_view_y(view_camera[0]));
		}
	}
	else if (index == 2) {
		draw_clear_alpha(c_black, 0);
		draw_set_circle_precision(64);
		draw_set_color(c_black);
		draw_set_alpha(1);
		draw_circle(device_mouse_x(0), device_mouse_y(0), radius, 0);
		//show_debug_message([device_mouse_x(0), device_mouse_y(0)]);

		gpu_set_blendmode_ext(bm_dest_alpha, bm_inv_src_alpha);
		draw_surface(this.getSurface(index - 1), 0, 0);
		gpu_set_blendmode(bm_normal);

		draw_set_color(c_white);
	}
	else if (index == 3) {
		draw_clear_alpha(c_black, 0);
		draw_surface_ext(this.getSurface(index - 1), device_mouse_x(0) * (1 - scale) - display_get_gui_width() * (camera_get_view_x(view_camera[0]) / camera_get_view_width(view_camera[0])), device_mouse_y(0) * (1 - scale) - display_get_gui_height()  * (camera_get_view_y(view_camera[0]) / camera_get_view_height(view_camera[0])), scale, scale, 0, c_white, 1);
	}
}

draw = function(this, surface, index) {
	if (index == 1 || index == 3) return;
	//show_debug_message("draw ok");
	if (!this.isRedraw) {
		draw_surface_ext(surface, device_mouse_x(0) * (1 - scale), device_mouse_y(0) * (1 - scale), scale, scale, 0, c_white, 1);
		this.isRedraw = true;
		this.samePlace = true;
	}
	//else {
	//	this.isRedraw = false;
	//}
}

drawGUI = function(this, surface, index) {
	if (index == 1 || index == 2) return;
	//show_debug_message("draw ok");
	/*
	if (this.isRedraw) {
		draw_surface_ext(surface, device_mouse_x_to_gui(0) * (1 - scale), device_mouse_y_to_gui(0) * (1 - scale), scale, scale, 0, c_white, 1);
		if (!this.samePlace) {
			this.isRedraw = false;
			this.samePlace = true;
		}
		else this.samePlace = false;
	}*/
	if (this.isRedraw) {
		if (this.samePlace) this.samePlace = false;
		else {
			//draw_surface_ext(surface, device_mouse_x_to_gui(0) * (1 - scale), device_mouse_y_to_gui(0) * (1 - scale), scale, scale, 0, c_white, 1);
			//var sprite = sprite_create_from_surface(surface, device_mouse_x_to_gui(0) * (1 - scale), device_mouse_y_to_gui(0) * (1 - scale), scale * 2, scale * 2, false, false, 0, 0);
			//draw_sprite(sprite, -1, device_mouse_x_to_gui(0) - scale, device_mouse_y_to_gui(0) - scale);
			/*
			var sprite = sprite_create_from_surface(surface, 0, 0, surface_get_width(surface), surface_get_height(surface), false, false, 0, 0);
			gpu_set_blendmode(bm_subtract)
			draw_sprite(sprite, -1, 0, 0);
			sprite_delete(sprite);
			gpu_set_blendmode(bm_normal);
			*/
			//draw_surface_ext(surface, device_mouse_x_to_gui(0) * (1 - scale), device_mouse_y_to_gui(0) * (1 - scale), scale, scale, 0, c_white, 1);
			draw_surface(surface, 0, 0);
			//draw_surface_ext(surface, 0, 0, scale, scale, 0, c_white, 1);
			this.isRedraw = false;
		}
	}
}


destroy = function(this, surface, index) {
	if (surface_exists(surface)) surface_free(surface);
}