create = function(this, surfaces) {
	ds_list_add(surfaces, 
		surface_create(room_width, room_height)
	);
	this.isRedraw = false;
	this.is_after_reset = true;
}

step = function(this, surface, index) {
	//var sprite = sprite_create_from_surface(application_surface,0,0,surface_get_width(application_surface),surface_get_height(application_surface),false,false,0,0);
	draw_clear_alpha(c_black, 0);
	this.isRedraw = true;
	//draw_sprite(sprite, -1, 0, 0)
	draw_surface(application_surface, camera_get_view_x(view_camera[0]), camera_get_view_y(view_camera[0]))
	this.isRedraw = false;
	//sprite_delete(sprite)
}

draw = function(this, surface, index) {
	if (!isRedraw) {
		gpu_set_blendmode_ext(bm_one, bm_zero);
		for (var i = 0; i < room_width; i += 2)
			draw_surface_part(surface, i * 2, 0, 2, room_height, i * 2, random_range(-5,5));
			
		gpu_set_blendmode(bm_normal);
	}
}