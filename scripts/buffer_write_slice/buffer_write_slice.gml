/// buffer_write_slice(buffer, data_buffer, data_start, data_end)
function buffer_write_slice(buffer, data_buffer, data_start, data_end) begin
	var next = data_end - data_start
	if (next > 0) begin
		var size = buffer_get_size(buffer)
		var pos = buffer_tell(buffer)
		var need = pos + next
		if size < need begin
			do size *= 2 until (size >= need)
			buffer_resize(buffer, size)
		end
		buffer_copy(data_buffer, data_start, next, buffer, pos)
		buffer_seek(buffer, buffer_seek_relative, next)
	end
end
