/// @description scrLoadGame(loadFile)
/// Loads the game
/// argument0 - sets whether or not to read the save file when loading the game
function scrLoadGame(loadFile) {

	// Only load save data from the save file if the script is currently set to (we should only need to read the save file on first load because the game stores them afterwards)
	if (loadFile) {
	    // Load the save map
    
	    //var f = file_text_open_read(/*"Data\\save"+string(global.saveNum)*/ global.gameName + "/DataSave" + string(global.saveNum) + ".bin");
	
		var f = /*file_text_open_read*/file_bin_open(global.gameName + "/dataSave"+string(global.saveNum) + ".bin",0);
		var result = "";
		var length = 0;
		length += file_bin_read_byte(f) * 1000000;
		length += file_bin_read_byte(f) * 10000;
		length += file_bin_read_byte(f) * 100;
		length += file_bin_read_byte(f);
		repeat(length) {
			result += (chr(file_bin_read_byte(f) ^ 178));
		}
		
	    var saveMap = json_decode(base64_decode(result));
		
		//var saveMap = ds_map_code_read(result);
		
		//show_debug_message(json_beautify(json_encode(saveMap)))
	
	    file_bin_close(f);
    
	    var saveValid = true; // Keeps track of whether or not the save being loaded is valid
    
	    if (saveMap != -1) { // Check if the save map loaded properly
	        global.deaths = ds_map_find_value(saveMap,"deaths");
	        global.time = ds_map_find_value(saveMap,"time");
	        global.timeMicro = ds_map_find_value(saveMap,"timeMicro");
        
	        global.difficulty = ds_map_find_value(saveMap,"difficulty");
	        global.saveRoom = ds_map_find_value(saveMap,"saveRoom");
	        global.savePlayerX = ds_map_find_value(saveMap,"savePlayerX");
	        global.savePlayerY = ds_map_find_value(saveMap,"savePlayerY");
	        global.saveGrav = ds_map_find_value(saveMap,"saveGrav");
        
	        if (is_string(global.saveRoom)) { // Check if the saved room string loaded properly
	            if (!room_exists(asset_get_index(global.saveRoom))) { // Check if the room index in the save is valid
	                saveValid = false;
				}
	        } else {
	            saveValid = false;
	        }
        
			//global.saveSecretItem = ds_map_find_value(saveMap,"saveSecretItem");
			//global.saveBossItem = ds_map_find_value(saveMap,"saveBossItem");
			
			//var item = ds_map_find_value(saveMap,"saveSecretItem");
			//var boss = ds_map_find_value(saveMap,"saveBossItem");
			
			//global.saveSecretItem = array_create(SECRET_ITEM_TOTAL);
			//global.saveBossItem = array_create(BOSS_ITEM_TOTAL);
			
			//for (var i = 0; i < SECRET_ITEM_TOTAL; ++i) global.saveSecretItem[i] = item[| i];
			//for (var i = 0; i < BOSS_ITEM_TOTAL; ++i) global.saveBossItem[i] = boss[| i];

	        //global.saveGameClear = ds_map_find_value(saveMap,"saveGameClear");
			
			{
				var itemCommonLength = ds_map_find_value(saveMap, "itemCommonLength");
				var itemDonateLength = ds_map_find_value(saveMap, "itemCommonLength");
				var itemTriggerLength = ds_map_find_value(saveMap, "itemTriggerLength");
				var itemMommentLength = ds_map_find_value(saveMap, "itemMommentLength");
				
				var itemCommon = base64_decode(ds_map_find_value(saveMap, "itemCommon"));
				//show_debug_message(itemCommon)
				for (var i = 0; i < itemCommonLength; ++i) {
					global.saveItem[? "Common"][| i] = (ord(string_char_at(itemCommon, i + 1)) == 2) ? true : false;
				}
				
				var itemDonate = base64_decode(ds_map_find_value(saveMap, "itemDonate"));
				//show_debug_message(itemDonate);
				for (var i = 0; i < itemDonateLength; ++i) {
					global.saveItem[? "Donate"][| i] = (ord(string_char_at(itemDonate, i + 1)) == 2) ? true : false;
				}
				
				var itemTrigger = base64_decode(ds_map_find_value(saveMap, "itemTrigger"));
				//show_debug_message(itemTrigger);
				for (var i = 0; i < itemTriggerLength; ++i) {
					global.saveItem[? "Trigger"][| i] = (ord(string_char_at(itemTrigger, i + 1)) == 2) ? true : false;
				}
				
				var itemMomment = base64_decode(ds_map_find_value(saveMap, "itemMomment"));
				//show_debug_message(itemMomment);
				for (var i = 0; i < itemMommentLength; ++i) {
					global.saveItem[? "Momment"][| i] = (ord(string_char_at(itemMomment, i + 1)) == 2) ? true : false;
				}
				
				var itemOtherKey = base64_decode(ds_map_find_value(saveMap, "itemOtherKey"));
				//show_debug_message(itemOtherKey);
				var itemOtherLength = ds_map_find_value(saveMap, "itemOtherLength");
				var beginning = false;
				var tmp_string = "";
				var result_list = ds_list_create();
				
				for (var i = 1; i <= itemOtherLength; ++i) {
					var str = string_char_at(itemOtherKey, i);
					if (beginning) {
						if (str == "0") {
							ds_list_add(result_list, tmp_string);
							tmp_string = "";
							beginning = false;
							continue;
						}
						tmp_string += chr(ord(str) ^ 178);
					} else {
						if (str == "1") {
							beginning = true;
						}
					}
				}
				
				beginning = false;
				var tmp_result = undefined;
				var itemOther = base64_decode(ds_map_find_value(saveMap, "itemOther"));
				show_debug_message(itemOther);
				var index = 0;
				
				var numful = false;
				var strful = false;
				
				var nummusful = false;
				
				for (var i = 0; i < itemOtherLength; ++i) {
					var str = string_char_at(itemOther, i);
					if (beginning) {
						if (str == "0") {
							if (nummusful) {
								tmp_result = -tmp_result;
							}
							global.saveItem[? "Other"][? result_list[| index]] = tmp_result;
							tmp_result = undefined;
							beginning = false;
							numful = false;
							strful = false;
							nummusful = false;
							++index;
						} 
						else if (str == "2") {
							tmp_result = 0;
							numful = true;
						}
						else if (str == "3") {
							tmp_result = "";
							strful = true;
						}
						else {
							if (numful) {
								if (str == "n") {
									nummusful = true;
								}
								else if (str == "a") {
									tmp_result += 1102500;
								}
								else {
									tmp_result += ord(str);
								}
							}
							else if (strful) {
								tmp_result += chr(ord(str) ^ 178);
							}
						}
					}
				}
			}
			
        
	        // Load MD5 string from the save map
	        var mapMd5 = ds_map_find_value(saveMap,"mapMd5");
			var mapSha1 = ds_map_find_value(saveMap, "mapSha1");
        
	        // Check if MD5 is not a string in case the save was messed with or got corrupted
	        if (!is_string(mapMd5) || !is_string(mapSha1)) {
	            saveValid = false; // MD5 is not a string, save is invalid
			} else {
		        // Generate MD5 string to compare with
		        ds_map_delete(saveMap,"mapMd5");
				ds_map_delete(saveMap,"mapSha1");
				
		        var genMd5 = md5_string_unicode(/*json_beautify(json_encode(saveMap))*/ds_map_code_write(saveMap) + MD5_STR_ADD);
				var genSha1 = sha1_string_unicode(/*json_beautify(json_encode(saveMap))*/ds_map_code_write(saveMap) + SHA1_STR_ADD);
        
				//show_debug_message(genMd5);
				//show_debug_message(genSha1);
				
				// Check if MD5 hash is invalid
		        if (mapMd5 != genMd5 || genSha1 != mapSha1) {
		            saveValid = false;
				}
			}
        
	        // Destroy the map
	        ds_map_destroy(saveMap);
	    } else {
	        // Save map didn't load correctly, set the save to invalid
	        saveValid = false;
	    }
    
	    if (!saveValid) { // Check if the save is invalid
	        // Save is invalid, restart the game
	        show_message("Save invalid!");
			game_restart();
	        exit;
	    }
	}

	// Set game variables and the player's position

	with (objPlayer) { // Destroy the player if it exists
	    instance_destroy();
	}

	global.gameStarted = true; // Sets game in progress (enables saving, restarting, etc.)
	global.noPause = false; // Disable no pause mode
	global.autosave = false; // Disable autosaving since we're loading the game

	global.grav = global.saveGrav;

	//array_copy(global.secretItem,0,global.saveSecretItem,0,SECRET_ITEM_TOTAL);
	//array_copy(global.bossItem,0,global.saveBossItem,0,BOSS_ITEM_TOTAL);
	
	ds_list_copy(global.item[? "Common"], global.saveItem[? "Common"])
	ds_list_copy(global.item[? "Donate"], global.saveItem[? "Donate"])
	ds_list_copy(global.item[? "Trigger"], global.saveItem[? "Trigger"])
	ds_list_copy(global.item[? "Momment"], global.saveItem[? "Momment"])
	ds_map_copy(global.item[? "Other"], global.saveItem[? "Other"]);
	
	//show_debug_message(json_beautify(json_encode(global.item)));

	//global.gameClear = global.saveGameClear;
	
	//show_debug_message(json_beautify(json_encode(global.item)))

	// Check if the player's layer exists, if it doesn't then create a temporary layer
	var spawnLayer = (layer_exists("Player")) ? layer_get_id("Player") : layer_create(0);
	instance_create_layer(global.savePlayerX,global.savePlayerY,spawnLayer,objPlayer);

	room_goto(asset_get_index(global.saveRoom));


}
