// v2.3.0的脚本资产已更改，请参见\ n // https://help.yoyogames.com/hc/en-us/articles/360005277377
function ds_map_code_write(map){
	
	var key = ds_map_keys_to_array(map);
	var length = array_length(key);
	
	var result = "";
	
	for (var i = 0; i < length; ++i) {
		result += "1";
		var str = key[i];
		for (var j = 1; j <= string_length(str); ++j) {
			result += chr(ord(string_char_at(str, j)) ^ 178);
		}
		result += "0";
	}
	
	result += "2";
	
	for (var i = 0; i < length; ++i) {
		result += "1";
		var item = map[? key[i]];
		if (is_int32(item) ) {
			item += "2";
			
			while(true) {
				if (item < 0) {
					item += "n";
					item = -item;
					continue;
				}
				if (item <= 1102500) {
					result += chr(item);
					break;
				}
				result += "a"
				item -= 1102500
			}
		}
		else if(is_bool(item)) {
			result += "2" + chr(1) + "0";
		}
		else if(is_string(item)) {
			result += "3";
			var len = string_length(item);
			for (var j=1;j<=len;++j) {
				result += chr(ord(string_char_at(item, j)) ^ 178);
			}
		}
		result += "0"
	}
	
	return base64_encode(result);
}