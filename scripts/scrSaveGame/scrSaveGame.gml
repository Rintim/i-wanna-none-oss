/// @description scrSaveGame(savePosition)
/// Saves the game
/// argument0 - sets whether the game should save the player's current location or just save the deaths/time
function scrSaveGame(argument0) {

	var savePosition = argument0;

	// Save the player's current location variables if the script is currently set to (we don't want to save the player's location if we're just updating death/time)
	if (savePosition) {
	    global.saveRoom = room_get_name(room);
	    global.savePlayerX = objPlayer.x;    
	    global.savePlayerY = objPlayer.y;
	    global.saveGrav = global.grav;
    
	    // Check if the player is saving inside of a wall or in the ceiling when the player's position is floored to prevent save locking
	    with (objPlayer) {
	        if (!place_free(floor(global.savePlayerX),global.savePlayerY)) {
	            global.savePlayerX += 1;
	        }
        
	        if (!place_free(global.savePlayerX,floor(global.savePlayerY))) {
	            global.savePlayerY += 1;
	        }
        
	        if (!place_free(floor(global.savePlayerX),floor(global.savePlayerY))) {
	            global.savePlayerX += 1;
	            global.savePlayerY += 1;
	        }
	    }
    
	    // Floor the player's position to match standard engine behavior
	    global.savePlayerX = floor(global.savePlayerX);
	    global.savePlayerY = floor(global.savePlayerY);
    
		//array_copy(global.saveSecretItem,0,global.secretItem,0,SECRET_ITEM_TOTAL);
		//array_copy(global.saveBossItem,0,global.bossItem,0,BOSS_ITEM_TOTAL);
		
		ds_list_copy(global.saveItem[? "Common"], global.item[? "Common"]);
		ds_list_copy(global.saveItem[? "Donate"], global.item[? "Donate"]);
		ds_list_copy(global.saveItem[? "Trigger"], global.item[? "Trigger"]);
		ds_list_copy(global.saveItem[? "Momment"], global.item[? "Momment"]);
		ds_map_copy(global.saveItem[? "Other"], global.item[? "Other"]);
    
	    //global.saveGameClear = global.gameClear;
	}
	
	//show_debug_message(json_beautify(json_encode(global.saveItem)));

	// Create a map for save data
	var saveMap = ds_map_create();

	ds_map_add(saveMap,"deaths",global.deaths);
	ds_map_add(saveMap,"time",global.time);
	ds_map_add(saveMap,"timeMicro",global.timeMicro);

	ds_map_add(saveMap,"difficulty",global.difficulty);
	ds_map_add(saveMap,"saveRoom",global.saveRoom);
	ds_map_add(saveMap,"savePlayerX",global.savePlayerX);
	ds_map_add(saveMap,"savePlayerY",global.savePlayerY);
	ds_map_add(saveMap,"saveGrav",global.saveGrav);
	
	ds_map_add(saveMap, "itemCommonLength", GAME_ITEM_COMMON_SIZE);
	ds_map_add(saveMap, "itemDonateLength", GAME_ITEM_DONATE_SIZE);
	ds_map_add(saveMap, "itemTriggerLength", GAME_ITEM_DONATE_SIZE);
	ds_map_add(saveMap, "itemMommentLength", GAME_ITEM_MOMMENT_SIZE);
	
	var commonString = "";
	for(var i = 0; i < GAME_ITEM_COMMON_SIZE; ++i) {
		commonString += chr(global.saveItem[? "Common"][| i]  + 1);
	}
	ds_map_add(saveMap, "itemCommon", base64_encode(commonString));
	//show_debug_message(commonString);
	
	var donateString = "";
	for(var i = 0; i < GAME_ITEM_DONATE_SIZE; ++i) {
		donateString += chr(global.saveItem[? "Donate"][| i]  + 1);
	}
	ds_map_add(saveMap, "itemDonate", base64_encode(donateString));
	//show_debug_message(donateString);
	
	var triggerString = "";
	for(var i = 0; i < GAME_ITEM_TRIGGER_SIZE; ++i) {
		triggerString += chr(global.saveItem[? "Trigger"][| i]  + 1);
	}
	ds_map_add(saveMap, "itemTrigger", base64_encode(triggerString));
	//show_debug_message(triggerString);
	
	var mommentString = "";
	for(var i = 0; i < GAME_ITEM_MOMMENT_SIZE; ++i) {
		mommentString += chr(global.saveItem[? "Momment"][| i] + 1);
	}
	ds_map_add(saveMap, "itemMomment", base64_encode(mommentString));
	//show_debug_message(mommentString);
	
	var other_key = ds_map_keys_to_array(global.saveItem[? "Other"]);
	var length = array_length(other_key);
	
	ds_map_add(saveMap, "itemOtherLength", length);
	
	var otherKeyString = "";
	
	for (var i = 0; i < length; ++i) {
		otherKeyString += "1";
		var str = other_key[i];
		for (var j = 1; j <= string_length(str); ++j) {
			otherKeyString += chr(ord(string_char_at(str, j)) ^ 178);
		}
		otherKeyString += "0";
	}
	
	ds_map_add(saveMap, "itemOtherKey", base64_encode(otherKeyString));
	//show_debug_message(otherKeyString);
	
	var otherString = "";
	
	for (var i = 0; i < length; ++i) {
		otherString += "1";
		var thisOther = global.saveItem[? "Other"][? other_key[i]];
		if (is_int32(thisOther) ) {
			otherString += "2";
			
			while(true) {
				if (thisOther < 0) {
					otherString += "n";
					thisOther = -thisOther;
					continue;
				}
				if (thisOther <= 1102500) {
					otherString += chr(thisOther);
					break;
				}
				otherString += "a"
				thisOther -= 1102500
			}
		}
		else if(is_bool(thisOther)) {
			otherString += "2" + chr(1) + "0";
		}
		else if(is_string(thisOther)) {
			otherString += "3";
			var len = string_length(thisOther);
			for (var j=1;j<=len;++j) {
				otherString += chr(ord(string_char_at(thisOther, j)) ^ 178);
			}
		}
		otherString += "0"
	}
	
	ds_map_add(saveMap, "itemOther", base64_encode(otherString));
	//show_debug_message(otherString);
	
	//ds_map_add(saveMap,"saveSecretItem",global.saveSecretItem);
	//ds_map_add(saveMap,"saveBossItem",global.saveBossItem);

	//ds_map_add(saveMap,"saveGameClear",global.saveGameClear);

	// Add MD5 hash to verify saves and make them harder to hack
	var mapMd5 = md5_string_unicode(/*json_beautify(json_encode(saveMap))*/ds_map_code_write(saveMap) + MD5_STR_ADD);
	var mapSha1 = sha1_string_unicode(/*json_beautify(json_encode(saveMap))*/ds_map_code_write(saveMap) + SHA1_STR_ADD);
	
	//show_debug_message(mapMd5);
	//show_debug_message(mapSha1);
	
	ds_map_add(saveMap,"mapMd5", mapMd5);
	ds_map_add(saveMap,"mapSha1", mapSha1);

	// Save the map to a file
	
	/*

	var f = file_text_open_write("Data\\save"+string(global.saveNum));
    
	file_text_write_string(f,base64_encode(ds_map_write(saveMap))); // Write map to the save file with base64 encoding
    
	file_text_close(f);

	// Destroy the map
	ds_map_destroy(saveMap);
	
	*/
	var result = base64_encode(json_beautify(json_encode(saveMap)) /*ds_map_write(saveMap)*/);
	//var result = ds_map_code_write(saveMap); /*ds_map_write(saveMap)*/
	
	var f = file_bin_open(global.gameName + "/dataSave"+string(global.saveNum) + ".bin",1);

	var length = string_length(result);

	var tmp = floor(length/1000000);
	file_bin_write_byte(f,tmp);
	length -= tmp*1000000;
	
	tmp = floor(length/10000);
	file_bin_write_byte(f,tmp);
	length -= tmp*10000;
	
	tmp = floor(length/100);
	file_bin_write_byte(f,tmp);
	length -= tmp*100;
	
	file_bin_write_byte(f,length);
	
	for (var i=1;i<=string_length(result);++i) {
		file_bin_write_byte(f,ord(string_char_at(result,i)) ^ 178);
	}
	file_bin_close(f);
	
	ds_map_destroy(saveMap);
}
