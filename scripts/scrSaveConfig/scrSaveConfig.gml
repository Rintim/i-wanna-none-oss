/// @description scrSaveConfig()
/// Saves current config settings
function scrSaveConfig() {

	/*
	ini_open("config.ini");

	// General settings
	ini_write_real("settings","mute_music",global.muteMusic);
	ini_write_real("settings","volume_level",global.volumeLevel);
	ini_write_real("settings","fullscreen_mode",global.fullscreenMode);
	ini_write_real("settings","smoothing_mode",global.smoothingMode);

	// Keyboard controls
	ini_write_real("controls","left",global.leftButton[0]);
	ini_write_real("controls","right",global.rightButton[0]);
	ini_write_real("controls","up",global.upButton[0]);
	ini_write_real("controls","down",global.downButton[0]);
	ini_write_real("controls","jump",global.jumpButton[0]);
	ini_write_real("controls","shoot",global.shootButton[0]);
	ini_write_real("controls","restart",global.restartButton[0]);
	ini_write_real("controls","skip",global.skipButton[0]);
	ini_write_real("controls","suicide",global.suicideButton[0]);
	ini_write_real("controls","pause",global.pauseButton[0]);
	ini_write_real("controls","align_left",global.alignLeftButton[0]);
	ini_write_real("controls","align_right",global.alignRightButton[0]);

	if (CONTROLLER_ENABLED) {
	    // Controller options/controls
	    ini_write_real("controller","index",global.controllerIndex);
	    ini_write_real("controller","left",global.leftButton[1]);
	    ini_write_real("controller","right",global.rightButton[1]);
	    ini_write_real("controller","up",global.upButton[1]);
	    ini_write_real("controller","down",global.downButton[1]);
	    ini_write_real("controller","jump",global.jumpButton[1]);
	    ini_write_real("controller","shoot",global.shootButton[1]);
	    ini_write_real("controller","restart",global.restartButton[1]);
	    ini_write_real("controller","skip",global.skipButton[1]);
	    ini_write_real("controller","suicide",global.suicideButton[1]);
	    ini_write_real("controller","pause",global.pauseButton[1]);
	    ini_write_real("controller","align_left",global.alignLeftButton[1]);
	    ini_write_real("controller","align_right",global.alignRightButton[1]);
	}

	ini_close();

	*/
	
	var map = ds_map_create();
	var setting = ds_map_create();
	ds_map_add(setting,"muteMusic",global.muteMusic);
	ds_map_add(setting,"volumeLevel",global.volumeLevel);
	ds_map_add(setting,"fullscreenMode",global.fullscreenMode);
	ds_map_add(setting,"smoothingMode",global.smoothingMode);
	var resolution = ds_list_create();
	ds_list_copy(resolution,global.resolution);
	ds_map_add_list(setting,"resolution",resolution);
	ds_map_add_map(map,"setting",setting);

	var controls = ds_map_create();
	ds_map_add(controls,"left",global.leftButton[0]);
	ds_map_add(controls,"right",global.rightButton[0]);
	ds_map_add(controls,"up",global.upButton[0]);
	ds_map_add(controls,"down",global.downButton[0]);
	ds_map_add(controls,"jump",global.jumpButton[0]);
	ds_map_add(controls,"shoot",global.shootButton[0]);
	ds_map_add(controls,"restart",global.restartButton[0]);
	ds_map_add(controls,"skip",global.skipButton[0]);
	ds_map_add(controls,"suicide",global.suicideButton[0]);
	ds_map_add(controls,"pause",global.pauseButton[0]);
	ds_map_add(controls,"align_left",global.alignLeftButton[0]);
	ds_map_add(controls,"align_right",global.alignRightButton[0]);
	ds_map_add_map(map,"controls",controls);

	if (CONTROLLER_ENABLED) {
		var controller = ds_map_create();
		ds_map_add(controller,"index",global.controllerIndex);
	    ds_map_add(controller,"left",global.leftButton[1]);
	    ds_map_add(controller,"right",global.rightButton[1]);
	    ds_map_add(controller,"up",global.upButton[1]);
	    ds_map_add(controller,"down",global.downButton[1]);
	    ds_map_add(controller,"jump",global.jumpButton[1]);
	    ds_map_add(controller,"shoot",global.shootButton[1]);
	    ds_map_add(controller,"restart",global.restartButton[1]);
	    ds_map_add(controller,"skip",global.skipButton[1]);
	    ds_map_add(controller,"suicide",global.suicideButton[1]);
	    ds_map_add(controller,"pause",global.pauseButton[1]);
	    ds_map_add(controller,"align_left",global.alignLeftButton[1]);
	    ds_map_add(controller,"align_right",global.alignRightButton[1]);
		ds_map_add_map(map,"controller",controller);
	}

	var json = json_beautify(json_encode(map));
	var buf = buffer_create(string_byte_length(json),buffer_fixed,1);
	buffer_write(buf,buffer_text,json);
		//buffer_tell(buff)
	buffer_save(buf, global.gameName + "/config.json");

	ds_map_destroy(map);
	
}
