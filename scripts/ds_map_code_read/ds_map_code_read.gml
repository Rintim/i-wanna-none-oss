// v2.3.0的脚本资产已更改，请参见\ n // https://help.yoyogames.com/hc/en-us/articles/360005277377
function ds_map_code_read(str){
	str = base64_decode(str);
	
	var beginning = false;
	var nexts = false;
	var tmp_result = undefined;
	var key_list = ds_list_create();
	var index = 0;	
	var result = ds_map_create();
	
	var str_length = string_length(str);
	
	var numful = false;
	var strful = false;
				
	var nummusful = false;
	
	for (var i = 1; i <= str_length; ++i) {
		var this = string_char_at(str, i);
		if (nexts) {
			if (beginning) {
				if (this == "0") {
					if (nummusful) {
						tmp_result = -tmp_result;
					}
					result[? key_list[| index]] = tmp_result;
					tmp_result = undefined;
					beginning = false;
					numful = false;
					strful = false;
					nummusful = false;
					++index;
				} 
				else if (this == "2") {
					tmp_result = 0;
					numful = true;
				}
				else if (this == "3") {
					tmp_result = "";
					strful = true;
				}
				else {
					if (numful) {
						if (this == "n") {
							nummusful = true;
						}
						else if (this == "a") {
							tmp_result += 1102500;
						}
						else {
							tmp_result += ord(this);
						}
					}
					else if (strful) {
						tmp_result += chr(ord(this) ^ 178);
					}
				}
			}
		}
		else {
			if (beginning) {
				if (this == "0") {
					ds_list_add(key_list, tmp_result);
					tmp_result = undefined;
					beginning = false;
					continue;
				}
				tmp_result += chr(ord(this) ^ 178);
			} else {
				if (this == "1") {
					beginning = true;
					tmp_result = "";
				}
				else if (this == "2") {
					nexts = true;
				}
			}
		}
	}
	
	ds_list_destroy(key_list);
	return result;
}