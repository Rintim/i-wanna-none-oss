/// @description Insert description here
// You can write your code in this editor

draw_set_font(fHannotate30);

//draw_set_halign(fa_left);

draw_set_color(c_black);

draw_text(64-7,180,"I");

draw_text(64+2,180+26*2,"W");
draw_text(64-1,180+26*3,"a");
draw_text(64-0.5,180+26*4,"n");
draw_text(64-0.5,180+26*5,"n");
draw_text(64-1,180+26*6,"a");

draw_text(64+1,180+26*8,"N");
draw_text(64-0.5,180+26*9,"o");
draw_text(64,180+26*10,"n");
draw_text(64,180+26*11,"e");

draw_set_font(fHannotate12);

draw_text(64+24-1, 180+26*8-12*2, "|");
draw_text(64+24-1, 180+26*8-12*1, "|");

draw_text(64+24, 180+26*8, "o");
draw_text(64+24, 180+26*8+12*1, "s");
draw_text(64+24, 180+26*8+12*2, "s");

draw_text(64+24, 180+26*8+12*4, "v");
draw_text(64+24, 180+26*8+12*5, "e");
draw_text(64+24, 180+26*8+12*6, "r");
draw_text(64+24, 180+26*8+12*7, "s");
draw_text(64+24-2, 180+26*8+12*8+2, "i");
draw_text(64+24, 180+26*8+12*9, "o");
draw_text(64+24, 180+26*8+12*10, "n");

// Development Branch Text

draw_set_halign(fa_left);

draw_set_color(c_gray);

draw_text(8,608 - 4,"Note: You are using Development Branch");

//draw_text(x + 64, y + 256, "I\n \nW\na\nn\nn\na\n \nN\no\nn\ne")