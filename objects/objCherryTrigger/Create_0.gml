// Set the sprite to not animate to prevent bouncing bugs

event_inherited();

image_speed = 1 / (scrGetFPS() / GAME_ORIGIN_FPS);