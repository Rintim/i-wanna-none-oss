/// @description Insert description here
// You can write your code in this editor
if(!beginning) {
	beginning = true;
	if(instance_exists(objPlayer)) objPlayer.frozen = true;
	if(instance_exists(objMapCreater)) objMapCreater.frozen = true;
	if (global.item[? "Donate"][| 0] == true) {
		objPlayer.frozen = false;
		objSnapCamera.frozen = false;
		objMapCreater.frozen = false;
		instance_destroy();
	}
}