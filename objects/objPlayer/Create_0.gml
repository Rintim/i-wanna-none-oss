/// @description Initialize variables

frozen = false; // Sets if the player can move or not

jump = 8.5 * global.grav / (scrGetFPS() / GAME_ORIGIN_FPS); // Sets how fast the player jumps
jump2 = 7 * global.grav / (scrGetFPS() / GAME_ORIGIN_FPS) ; // Sets how fast the player double jumps
originGravity = 0.4 * global.grav / power(scrGetFPS() / GAME_ORIGIN_FPS, 2); // Sets the player's gravity

gravity = originGravity;
unaGravity = false;

image_blend = c_white;


djump = 1; // Allow the player to double jump as soon as he spawns
maxHSpeed = 3 / (scrGetFPS() / GAME_ORIGIN_FPS); // Max horizontal speed
maxVSpeed = 9 / (scrGetFPS() / GAME_ORIGIN_FPS); // Max vertical speed
onPlatform = false; // Sets if the player is currently standing on a platform
jumpAble = true;
jumpTwiceAble = true;
jumpInfiniteAble = false;
shootAble = true;
rightAble = true;
leftAble = true;

image_speed = 1 / (scrGetFPS() / GAME_ORIGIN_FPS);

xScale = 1; // Sets the direction the player is facing (1 is facing right, -1 is facing left)

// Set the player's hitbox depending on gravity direction
if (global.grav == 1) {
	mask_index = sprPlayerMask;
} else {
	mask_index = sprPlayerMaskFlip;
}

// Create the player's bow if on medium mode
//if (global.difficulty == 0 && global.gameStarted) {
    instance_create_depth(x,y,depth-1,objBow);
//}

// Save the game if currently set to autosave
if (global.autosave) {
    scrSaveGame(true);
    global.autosave = false;
}