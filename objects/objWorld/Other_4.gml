/// @description Play current room music and put into the correct layer

scrGetMusic(); // Find and play the proper music for the current room

game_set_speed(GAME_FPS, gamespeed_fps);

game_speed = game_get_speed(gamespeed_fps);

layer = layer_get_id("World"); // Put into the correct layer