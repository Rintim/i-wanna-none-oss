/// @description Insert description here
// You can write your code in this editor

if (!beginning) {
	create(self, surfaces);
	beginning = true;
}

if (beginning) {
	for (var i = 0; i < ds_list_size(surfaces); ++i) {
		if (!surface_exists(surfaces[| i])) {
			surfaces[| i] = reCreate(self, i + 1);/*surface_create(room_width, room_height)*/;
		}
		surface_set_target(surfaces[| i]);
		step(self, surfaces[| i], i + 1);
		if (is_after_reset) surface_reset_target();
	}
	if (!is_after_reset) {
		repeat(ds_list_size(surfaces)) {
			surface_reset_target();
		}
	};
}

