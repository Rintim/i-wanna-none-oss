/// @description Insert description here
// You can write your code in this editor

surfaces = ds_list_create();

beginning = false;

getSurface = function(index) {
	return surfaces[| index - 1];
}

{ // Definable Varible
	
	create = function(this, surfaces) {
		ds_list_add(surfaces, surface_create(room_width, room_height));
	}
	
	reCreate = function(this, index) {
		return surface_create(room_width, room_height);
	}

	step = function(this, surface, index) {
		return;
	}

	draw = function(this, surface, index) {
		draw_surface(surface, 0, 0);
	}

	destroy = function(this, surface, index) {
		if (surface_exists(surface)) surface_free(surface);
	}
	
	drawGUI = function(this, surface, index) {
		return;
	}
	
	is_after_reset = false;

}
