/// @description Initiailze menu variables

xSelector = 96; // Sets where to draw the save info on the x axis
ySelector = 160; // Sets where to draw the save info on the y axis
xSeperation = 239; // Sets how much to separate the save files in the menu

// Stores the image index of the menu sprites
appleIndex = 0;
playerIndex = 0;

fileSelect = global.menuSelectPrev[0]; // Keeps track of what file we're currently on
difSelectMode = false; // Keeps track of whether we're currently selecting a difficulty
difSelect = 0; // Keeps track of what difficulty we're currently on

warnText = false; // Keeps track of whether the file overwrite warning text is showing
warnSelect = true; // Keeps track of whether to accept overwriting the save file

// Load save file values

for (var i = 0; i < 3; i++) {
    if (file_exists(global.gameName + "/dataSave"+string(i + 1) + ".bin")) { // Check if the current save exists
        // Save exists, load save data
        exists[i] = true;
        
        // Load the save map
        var f = /*file_text_open_read*/file_bin_open(global.gameName + "/dataSave"+string(i + 1) + ".bin",0);
		var result = "";
		var length = 0;
		length += file_bin_read_byte(f) * 1000000;
		length += file_bin_read_byte(f) * 10000;
		length += file_bin_read_byte(f) * 100;
		length += file_bin_read_byte(f);
		repeat(length) {
			result += (chr(file_bin_read_byte(f) ^ 178));
		}
	
	    //ds_map_read(saveMap,base64_decode(result));
		
		string_replace_all(result,chr(9),"");
		string_replace_all(result,chr(10),"");
		
	    var saveMap = json_decode(base64_decode(result));

	
	    file_bin_close(f);
        
        if (saveMap != -1) { // Check if the save map loaded correctly
            deaths[i] = ds_map_find_value(saveMap,"deaths");
			// Check for undefined values in case the save was messed with or got corrupted
            if (is_undefined(deaths[i])) {
                deaths[i] = 0;
			}
            
            time[i] = ds_map_find_value(saveMap,"time");
            if (is_undefined(time[i])) {
                time[i] = 0;
			}
    
            difficulty[i] = ds_map_find_value(saveMap,"difficulty");
            if (is_undefined(difficulty[i])) {
                difficulty[i] = 0;
			}
			
			var momment = base64_decode(ds_map_find_value(saveMap, "itemMomment"));
			if (is_undefined(momment)) {
				clear[i] = false;
				difficult[i] = false;
			}
			else {
				clear[i] = ord(string_char_at(momment, 1)) == 2 ? true : false;
				difficult[i] = ord(string_char_at(momment, 2)) == 2 ? true : false;
			}
            
            ds_map_destroy(saveMap);
        } else {
            // Save map didn't load correctly, set variables to the defaults
            deaths[i] = 0;
            time[i] = 0;
            difficulty[i] = 0;
			clear[i] = false;
			difficult[i] = false;
        }
    } else {
        exists[i] = false;
        deaths[i] = 0;
        time[i] = 0;
		clear[i] = false;
		difficult[i] = false;
    }
    
	// Convert the time into a string
    var t = time[i];
    
    timeStr[i] = string(t div 3600) + ":";
    t = t mod 3600;
    timeStr[i] += string(t div 600);
    t = t mod 600;
    timeStr[i] += string(t div 60) + ":";
    t = t mod 60;
    timeStr[i] += string(t div 10);
    t = t mod 10;
    timeStr[i] += string(floor(t));
}