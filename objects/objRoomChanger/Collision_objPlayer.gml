/// @description Warp the player

pre_do(self);

if (warpX == 0 && warpY == 0) { // No coordinates set, go to where objPlayerStart is
    with(objPlayer) {
        instance_destroy();
	}
} else { // Coordinates set, move player to them
    objPlayer.x = is_method(warpX) ? warpX(self) : warpX;
    objPlayer.y = is_method(warpY) ? warpY(self) : warpY;
	show_debug_message([objPlayer.x, objPlayer.y]);
}

room_goto(roomTo);

after_do(self);